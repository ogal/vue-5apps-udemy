// https://official-joke-api.appspot.com/jokes/ten
import * as types from './mutation-types'

// Creamos la primera acción
export const initJokes = ({ commit }) => {
  fetch('https://official-joke-api.appspot.com/jokes/ten', {
  method: 'GET'
  })
  .then( response => response.json() )
  .then( json => commit(types.INIT_JOKES, json) )
}

/**  Paso2, venimos del archivo mutations, ahora creamos nuestra acción,
Nos fijamos que llamamos a la mutación en el commit */
export const addJoke = ({ commit }) => {
  fetch('https://official-joke-api.appspot.com/jokes/random', {
  method: 'GET'
  })
  .then( response => response.json() )
  // Llamada a la mutación
  .then( json => commit(types.ADD_JOKE, json) )
}

export const removeJoke = ({ commit }, index ) => {
  commit(types.REMOVE_JOKE, index)
}
