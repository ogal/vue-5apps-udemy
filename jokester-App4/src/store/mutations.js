import * as types from './mutation-types'

export const mutations = {
  [types.INIT_JOKES] (state, payload) {
    state.jokes.push(...payload)
  },
  // Paso1, creamos una mutación, ahora vamos a actions.js
  [types.ADD_JOKE] (state, payload) {
    state.jokes.push(payload)
  },
  // El indice nos servirá para indicar que Joke queremos eliminar
  [types.REMOVE_JOKE] (state, index) {
    state.jokes.splice(index, 1)
  }
}
