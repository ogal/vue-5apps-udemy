import Vue from 'vue'
import App from './components/App.vue'

import store from './store'

/* Comprobamos que conecta con la constante del index.js de VueX */
// store.commit('INIT_JOKES', [{ test: 'test_joke'}, { test_2: 'test_joke_2' }])

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
