import Vue from 'vue'
import App from './App.vue'

// Definimos la instancia de vue, la letra h es por convención
new Vue({
  el: '#app',
  render: h => h(App)
})
